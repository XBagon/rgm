## **RGM**
A simple register machine simulator

#### Installation
```sh
cargo +beta install rgm
```

#### Usage
```sh
rgm 0.2.0
XBagon <xbagon@outlook.de>
A Register Machine Simulator

USAGE:
    rgm.exe [FLAGS] [OPTIONS] <input>

FLAGS:
    -d, --debug      Prints debug information for each step
    -h, --help       Prints help information
    -V, --version    Prints version information

OPTIONS:
    -c, --config <config>...    Allows to set initial configuration of registers
                                USAGE: index:value index:value index:value ...

ARGS:
    <input>    Input rgm file
```

**rgm** files are made of instructions seperated by newlines.

### Instructions

|Name|Params|Action|PC|
|----|------|------|---|
|LOAD|**i**|r(0)=r(**i**)|+=1|
|STORE|**i**|r(**i**)=r(0)|+=1|
|ADD|**i**|r(0)=r(0)+r(**i**)|+=1|
|SUB|**i**|r(0)=r(0)-r(**i**)|+=1|
|MULT|**i**|r(0)=r(0)*r(**i**)|+=1|
|DIV|**i**|r(0)=r(0)/r(**i**)|+=1|
|GOTO|**j**||= **j**|

|Name|Params|Usage|
|----|------|-----|
|CONDGOTO|**t**[=,≤,≥,<,>], **l**, **j**|IF r(0) **t** **l** GOTO **j**|

|Name|Params|Action|PC|
|----|------|------|---|
|CLOAD|**l**|r(0)=**l**|+=1|
|CADD|**l**|r(0)=r(0)+**l**|+=1|
|CSUB|**l**|r(0)=r(0)-**l**|+=1|
|CMULT|**l**|r(0)=r(0)***l**|+=1|
|CDIV|**l**|r(0)=r(0)/**l**|+=1|
|INDLOAD|**i**|r(0)=r(r(**i**))|+=1|
|INDSTORE|**i**|r(r(**i**))=r(0)|+=1|
|INDADD|**i**|r(0)=r(0)+r(r(**i**))|+=1|
|INDSUB|**i**|r(0)=r(0)-r(r(**i**))|+=1|
|INDMULT|**i**|r(0)=r(0)*r(r(**i**))|+=1|
|INDDIV|**i**|r(0)=r(0)/r(r(**i**))|+=1|