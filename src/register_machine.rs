use crate::instructions::Instruction;
use num_bigint::BigUint;
use num_traits::identities::Zero;
use std::{
    collections::HashMap,
    fmt::{self, Display},
};

#[derive(Default)]
pub struct RegisterMachine {
    pub program_counter: usize,
    pub accumulator: BigUint,
    pub register: HashMap<BigUint, BigUint>,
}

impl Display for RegisterMachine {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let mut display_for_register = String::new();
        let mut registers: Vec<_> = self.register.iter().collect();
        registers.sort_unstable_by_key(|kv| kv.0);
        for register in registers {
            display_for_register.push_str(&format!("{}:{} ", register.0, register.1));
        }
        write!(
            f,
            "PC = {}, Accumulator = {}\nRegisters = {}",
            self.program_counter, self.accumulator, display_for_register
        )
    }
}

impl RegisterMachine {
    pub fn execute(&mut self, instructions: &Vec<Box<dyn Instruction>>, debug: bool) {
        loop {
            if self.program_counter >= instructions.len() {
                println!("{}", self);
                std::process::exit(0); //To make the hacky End implementation consistent
            } else {
                if debug {
                    println!("Instruction: {}\n{}\n", instructions[self.program_counter], self);
                }
                instructions[self.program_counter].execute(self);
            }
        }
    }

    pub fn get(&mut self, i: &BigUint) -> BigUint {
        if i.is_zero() {
            self.accumulator.clone()
        } else if let Some(c_i) = self.register.get(i) {
            c_i.clone()
        } else {
            BigUint::default()
        }
    }

    pub fn set(&mut self, i: &BigUint) {
        if i.is_zero() {
            //rm.accumulator = rm.accumulator;
        } else {
            self.register.insert(i.clone(), self.accumulator.clone());
        }
    }
}
