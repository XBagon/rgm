#[macro_use]
extern crate pest_derive;
#[macro_use]
extern crate derive_more;

mod instructions;
mod parser;
mod register_machine;

use crate::{parser::*, register_machine::RegisterMachine};
use num_bigint::{BigUint, ParseBigIntError};
use num_traits::Num;
use pest::Parser;
use std::{
    fmt::{self, Display},
    fs,
    path::PathBuf,
};
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "rgm", about = "A Register Machine Simulator")]
struct Opt {
    #[structopt(short = "d", long = "debug", help = "Prints debug information for each step")]
    pub debug: bool,
    #[structopt(parse(from_os_str), help = "Input rgm file")]
    pub input: PathBuf,
    #[structopt(short = "c", long = "config", help = "Allows to set initial configuration of registers\nUSAGE: index:value index:value index:value ...")]
    pub config: Vec<BigUintTuple>,
}

#[derive(Debug)]
struct BigUintTuple(BigUint, BigUint);

#[derive(Debug)]
struct ParseBigUintTupleError {
    kind: BigUintTupleErrorKind,
}

#[derive(Debug)]
enum BigUintTupleErrorKind {
    TupleError,
    BigUintError(ParseBigIntError),
}

impl Display for ParseBigUintTupleError {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        match &self.kind {
            BigUintTupleErrorKind::TupleError => write!(f, "Has to be a set of 2 integers serperated by a ':'"),
            BigUintTupleErrorKind::BigUintError(e) => e.fmt(f),
        }
    }
}

impl std::convert::From<ParseBigIntError> for ParseBigUintTupleError {
    fn from(error: ParseBigIntError) -> Self {
        ParseBigUintTupleError {
            kind: BigUintTupleErrorKind::BigUintError(error),
        }
    }
}

impl std::str::FromStr for BigUintTuple {
    type Err = ParseBigUintTupleError;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let integers: Vec<_> = s.split(':').collect();
        if integers.len() != 2 {
            Err(ParseBigUintTupleError {
                kind: BigUintTupleErrorKind::TupleError,
            })
        } else {
            Ok(BigUintTuple(BigUint::from_str_radix(integers[0], 10)?, BigUint::from_str_radix(integers[1], 10)?))
        }
    }
}

fn main() -> Result<(), &'static str> {
    let opt = Opt::from_args();
    let input = fs::read_to_string(opt.input).unwrap();
    let syntax_tree = parser::RgmParser::parse(Rule::Program, &input).unwrap();
    let instructions = parser::parse(syntax_tree);
    let mut rm = RegisterMachine::default();

    for cfg in opt.config {
        rm.register.insert(cfg.0, cfg.1);
    }

    rm.execute(&instructions, opt.debug);

    Ok(())
}
