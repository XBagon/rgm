#[derive(Parser)]
#[grammar = "grammar.pest"]
pub struct RgmParser;

const _GRAMMAR: &str = include_str!("grammar.pest");

use crate::instructions::{Instruction, *};
use num_bigint::BigUint;
use pest::iterators::Pairs;

pub fn parse(mut syntax_tree: Pairs<Rule>) -> Vec<Box<dyn Instruction>> {
    let mut instructions: Vec<Box<dyn Instruction>> = Vec::new();

    let program = syntax_tree.next().unwrap();
    for instruction in program.into_inner() {
        instructions.push(match instruction.as_rule() {
            Rule::Load => Box::new(Load {
                i: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::Store => Box::new(Store {
                i: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::Add => Box::new(Add {
                i: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::Sub => Box::new(Sub {
                i: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::Mult => Box::new(Mult {
                i: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::Div => Box::new(Div {
                i: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::Goto => Box::new(Goto {
                j: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::CondGoto => {
                let mut inner = instruction.into_inner();
                Box::new(CondGoto {
                    typ: inner.next().unwrap().as_rule(),
                    l: BigUint::parse_bytes(inner.next().unwrap().as_str().as_bytes(), 10).unwrap(),
                    j: BigUint::parse_bytes(inner.next().unwrap().as_str().as_bytes(), 10).unwrap(),
                })
            }
            Rule::End | Rule::EOI => Box::new(End),
            Rule::CLoad => Box::new(CLoad {
                l: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::CAdd => Box::new(CAdd {
                l: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::CSub => Box::new(CSub {
                l: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::CMult => Box::new(CMult {
                l: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::CDiv => Box::new(CDiv {
                l: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::IndLoad => Box::new(IndLoad {
                i: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::IndStore => Box::new(IndStore {
                i: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::IndAdd => Box::new(IndAdd {
                i: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::IndSub => Box::new(IndSub {
                i: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::IndMult => Box::new(IndMult {
                i: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            Rule::IndDiv => Box::new(IndDiv {
                i: BigUint::parse_bytes(instruction.into_inner().as_str().as_bytes(), 10).unwrap(),
            }),
            _ => unreachable!(),
        });
    }
    instructions
}
