use crate::{parser::*, register_machine::RegisterMachine};
use num_bigint::BigUint;
use num_traits::cast::ToPrimitive;
use std::fmt::Display;

#[derive(Display)]
#[display(fmt = "Load {}", i)]
pub struct Load {
    pub i: BigUint,
}

#[derive(Display)]
#[display(fmt = "Store {}", i)]
pub struct Store {
    pub i: BigUint,
}

#[derive(Display)]
#[display(fmt = "Add {}", i)]
pub struct Add {
    pub i: BigUint,
}

#[derive(Display)]
#[display(fmt = "Sub {}", i)]
pub struct Sub {
    pub i: BigUint,
}

#[derive(Display)]
#[display(fmt = "Mult {}", i)]
pub struct Mult {
    pub i: BigUint,
}

#[derive(Display)]
#[display(fmt = "Div {}", i)]
pub struct Div {
    pub i: BigUint,
}

#[derive(Display)]
#[display(fmt = "Goto {}", j)]
pub struct Goto {
    pub j: BigUint,
}

#[derive(Display)]
#[display(fmt = "CondGoto {:?} {} Goto {}", typ, l, j)]
pub struct CondGoto {
    pub typ: Rule,
    pub l: BigUint,
    pub j: BigUint,
}

#[derive(Display)]
#[display(fmt = "End")]
pub struct End;

#[derive(Display)]
#[display(fmt = "CLoad {}", l)]
pub struct CLoad {
    pub l: BigUint,
}

#[derive(Display)]
#[display(fmt = "CAdd {}", l)]
pub struct CAdd {
    pub l: BigUint,
}

#[derive(Display)]
#[display(fmt = "CSub {}", l)]
pub struct CSub {
    pub l: BigUint,
}

#[derive(Display)]
#[display(fmt = "CMult {}", l)]
pub struct CMult {
    pub l: BigUint,
}

#[derive(Display)]
#[display(fmt = "CDiv {}", l)]
pub struct CDiv {
    pub l: BigUint,
}

#[derive(Display)]
#[display(fmt = "IndLoad {}", i)]
pub struct IndLoad {
    pub i: BigUint,
}

#[derive(Display)]
#[display(fmt = "IndStore {}", i)]
pub struct IndStore {
    pub i: BigUint,
}

#[derive(Display)]
#[display(fmt = "IndAdd {}", i)]
pub struct IndAdd {
    pub i: BigUint,
}

#[derive(Display)]
#[display(fmt = "IndSub {}", i)]
pub struct IndSub {
    pub i: BigUint,
}

#[derive(Display)]
#[display(fmt = "IndMult {}", i)]
pub struct IndMult {
    pub i: BigUint,
}

#[derive(Display)]
#[display(fmt = "IndDiv {}", i)]
pub struct IndDiv {
    pub i: BigUint,
}

pub trait Instruction: Display {
    fn execute(&self, rm: &mut RegisterMachine);
}

impl Instruction for Load {
    fn execute(&self, rm: &mut RegisterMachine) {
        rm.accumulator = rm.get(&self.i);
        rm.program_counter += 1;
    }
}

impl Instruction for Store {
    fn execute(&self, rm: &mut RegisterMachine) {
        rm.set(&self.i);
        rm.program_counter += 1;
    }
}

impl Instruction for Add {
    fn execute(&self, rm: &mut RegisterMachine) {
        let to_add = rm.get(&self.i).clone();
        rm.accumulator += to_add;
        rm.program_counter += 1;
    }
}

impl Instruction for Sub {
    fn execute(&self, rm: &mut RegisterMachine) {
        let to_sub = rm.get(&self.i).clone();
        rm.accumulator -= to_sub;
        rm.program_counter += 1;
    }
}

impl Instruction for Mult {
    fn execute(&self, rm: &mut RegisterMachine) {
        let to_mult = rm.get(&self.i).clone();
        rm.accumulator *= to_mult;
        rm.program_counter += 1;
    }
}

impl Instruction for Div {
    fn execute(&self, rm: &mut RegisterMachine) {
        let to_div = rm.get(&self.i).clone();
        rm.accumulator /= to_div;
        rm.program_counter += 1;
    }
}

impl Instruction for Goto {
    fn execute(&self, rm: &mut RegisterMachine) {
        rm.program_counter = self.j.to_usize().unwrap() - 1;
    }
}

impl Instruction for CondGoto {
    fn execute(&self, rm: &mut RegisterMachine) {
        rm.program_counter += 1;
        match self.typ {
            Rule::Eq => {
                if rm.accumulator == self.l {
                    rm.program_counter = self.j.to_usize().unwrap() - 1;
                }
            }
            Rule::Ste => {
                if rm.accumulator <= self.l {
                    rm.program_counter = self.j.to_usize().unwrap() - 1;
                }
            }
            Rule::Gte => {
                if rm.accumulator >= self.l {
                    rm.program_counter = self.j.to_usize().unwrap() - 1;
                }
            }
            Rule::St => {
                if rm.accumulator < self.l {
                    rm.program_counter = self.j.to_usize().unwrap() - 1;
                }
            }
            Rule::Gt => {
                if rm.accumulator > self.l {
                    rm.program_counter = self.j.to_usize().unwrap() - 1;
                }
            }
            _ => unreachable!(),
        }
    }
}

impl Instruction for End {
    fn execute(&self, rm: &mut RegisterMachine) {
        //A bit hacky
        println!("{}", rm);
        std::process::exit(0);
    }
}

impl Instruction for CLoad {
    fn execute(&self, rm: &mut RegisterMachine) {
        rm.accumulator = self.l.clone();
        rm.program_counter += 1;
    }
}

impl Instruction for CAdd {
    fn execute(&self, rm: &mut RegisterMachine) {
        rm.accumulator += self.l.clone();
        rm.program_counter += 1;
    }
}

impl Instruction for CSub {
    fn execute(&self, rm: &mut RegisterMachine) {
        rm.accumulator -= self.l.clone();
        rm.program_counter += 1;
    }
}

impl Instruction for CMult {
    fn execute(&self, rm: &mut RegisterMachine) {
        rm.accumulator *= self.l.clone();
        rm.program_counter += 1;
    }
}

impl Instruction for CDiv {
    fn execute(&self, rm: &mut RegisterMachine) {
        rm.accumulator /= self.l.clone();
        rm.program_counter += 1;
    }
}

impl Instruction for IndLoad {
    fn execute(&self, rm: &mut RegisterMachine) {
        let ind = &rm.get(&self.i);
        rm.accumulator = rm.get(ind);
        rm.program_counter += 1;
    }
}

impl Instruction for IndStore {
    fn execute(&self, rm: &mut RegisterMachine) {
        let ind = &rm.get(&self.i);
        rm.set(ind);
        rm.program_counter += 1;
    }
}

impl Instruction for IndAdd {
    fn execute(&self, rm: &mut RegisterMachine) {
        let ind = &rm.get(&self.i);
        let to_add = rm.register.get(ind).unwrap().clone();
        rm.accumulator += to_add;
        rm.program_counter += 1;
    }
}

impl Instruction for IndSub {
    fn execute(&self, rm: &mut RegisterMachine) {
        let ind = &rm.get(&self.i);
        let to_sub = rm.register.get(ind).unwrap().clone();
        rm.accumulator -= to_sub;
        rm.program_counter += 1;
    }
}

impl Instruction for IndMult {
    fn execute(&self, rm: &mut RegisterMachine) {
        let ind = &rm.get(&self.i);
        let to_mult = rm.register.get(ind).unwrap().clone();
        rm.accumulator *= to_mult;
        rm.program_counter += 1;
    }
}

impl Instruction for IndDiv {
    fn execute(&self, rm: &mut RegisterMachine) {
        let ind = &rm.get(&self.i);
        let to_div = rm.register.get(ind).unwrap().clone();
        rm.accumulator /= to_div;
        rm.program_counter += 1;
    }
}
